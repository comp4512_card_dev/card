package hk.edu.polyu.comp4512.card;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

import hk.edu.polyu.comp4512.card.cards_database.Card;

/**
 * Created by hopkins on 30/11/2017.
 */

public class CardDrawer {
    public static ImageView drawOnImageView(Card card, ImageView imageView, boolean halfTransparent, String drawText, int drawColor, Activity activity) {
        if(card.getFaceUpDrawable(activity.getResources()) instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable)card.getFaceUpDrawable(activity.getResources());
            if(bitmapDrawable.getBitmap() != null) {
                Bitmap cardBitmap = bitmapDrawable.getBitmap();
                Bitmap bitmapWithText = Bitmap.createBitmap(cardBitmap.getWidth(), cardBitmap.getHeight(), cardBitmap.getConfig());

                Canvas canvas = new Canvas(bitmapWithText);
                Paint alphaPaint = new Paint();
                alphaPaint.setAlpha(85);
                if(halfTransparent) {
                    canvas.drawBitmap(cardBitmap, 0, 0, alphaPaint);
                }else{
                    canvas.drawBitmap(cardBitmap, 0, 0, null);
                }

                Paint paint = new Paint();
                paint.setColor(drawColor);
                paint.setStyle(Paint.Style.FILL_AND_STROKE);
                paint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
                paint.setTextSize(99);

                canvas.drawText(drawText, 40, 990, paint);
                imageView.setImageBitmap(bitmapWithText);
            }else{
                imageView.setImageDrawable(card.getFaceUpDrawable(activity.getResources()));
            }
        }else{
            imageView.setImageDrawable(card.getFaceUpDrawable(activity.getResources()));
        }
        return imageView;
    }

    public static void getOpponentLinearLayout(ArrayList<Card> cards, LinearLayout linearLayout, Activity activity) {
        getOpponentLinearLayout(cards, linearLayout, activity, false, -1, false, -1,null);
    }

    public static void getOpponentLinearLayout(ArrayList<Card> cards, LinearLayout linearLayout, final Activity activity, final boolean espionage, final int espionageTarget, final boolean suing, final int sueTarget, final Dialog originalDialog) {

        int count=0;
        for (Card card : cards) {
            ImageView imageView = new ImageView(activity);
            imageView.setAdjustViewBounds(true);

            if(!espionage && !suing) { // Normal view (view faced up and face down)
                if (card.cardFacedUp()) {
                    // Draw the actual card
                    if (card.getCardStatus() == Card.CARD_STATUS.PATENTED) {
                        imageView = CardDrawer.drawOnImageView(card, imageView, false, "PATENTED", Color.GREEN, activity); // face up
                    } else if (card.getCardStatus() == Card.CARD_STATUS.PATENT_PENDING) {
                        imageView = CardDrawer.drawOnImageView(card, imageView, false, "PATENT PENDING", Color.RED, activity); // face up
                    } else if (card.getCardStatus() == Card.CARD_STATUS.OPEN_SOURCED) {
                        imageView = CardDrawer.drawOnImageView(card, imageView, false, "OPEN SOURCED", Color.BLACK, activity); // face up
                    }
                } else {
                    imageView.setImageDrawable(card.getFaceDownDrawable(activity.getResources()));
                }
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(20, 0, 20, 0);
                linearLayout.addView(imageView, layoutParams);
            }else{ // Filtered view (only show face down, which is TS) for suing/espionage
                // Only draw face down (Trade Secret), we can't espionage//sue cards that are OS/Patented/Patent Pending
                if(!card.cardFacedUp()) {
                    imageView.setImageDrawable(card.getFaceDownDrawable(activity.getResources()));
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.setMargins(20, 0, 20, 0);
                    linearLayout.addView(imageView, layoutParams);

                    final int thisCardCount=count;
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Perform espionage
                            if(espionage) {
                                ((GameActivity) activity).PerformEspionageOnSelectedTarget(espionageTarget, thisCardCount);
                            }

                            if(suing) {
                                ((GameActivity) activity).PerformSuingOnSelectedTarget(sueTarget, thisCardCount);
                            }
                            originalDialog.dismiss();
                        }
                    });

                }
            }
            count++;
        }
    }
}
