package hk.edu.polyu.comp4512.card.cards_database;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by hopkins on 29/11/2017.
 */

public class Database {
    private ArrayList<Card> listOfCards;


    public Database() {
        this.listOfCards = new ArrayList<>();
    }

    public void addCard(Card c) {
        this.listOfCards.add(c);
    }

    public void ramdomize() {
        Collections.shuffle(listOfCards);
    }

    public int getSize() {
        return listOfCards.size();
    }

    public ArrayList<Card> getCards() {
        return listOfCards;
    }

    public ArrayList<Card> getFacedUpCards() {
        ArrayList<Card> facedUpCards = new ArrayList<>();
        for(Card card : listOfCards) {
            if(card.cardFacedUp()) {
                facedUpCards.add(card);
            }
        }
        return facedUpCards;
    }

    public boolean facedUpDuplicated(Card cardToCheck) {
        ArrayList<Card> cards = getFacedUpCards();
        for(Card card : cards) {
            if(card.getWorth() == cardToCheck.getWorth() && card.getColor() == cardToCheck.getColor()) {
                return true;
            }
        }
        return false;
    }

    public int countCard(Card cardToCheck) {
        int occurrence=0;
        for(Card card : listOfCards) {
            if(card.getWorth() == cardToCheck.getWorth() && card.getColor() == cardToCheck.getColor()) {
                occurrence++;
            }
        }
        return occurrence;
    }

    public Card drawOneCard() {
        Card card = listOfCards.get(0);
        listOfCards.remove(0);
        card.setCardStatus(Card.CARD_STATUS.TRADE_SECRET);
        return card;
    }


    public static Database generateDefaultDeck() {
        Database database = new Database();
        database.addCard(new Card(Card.CARD_COLOR.RED, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.RED, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.RED, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.RED, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.RED, Card.CARD_WORTH.THREE));
        database.addCard(new Card(Card.CARD_COLOR.RED, Card.CARD_WORTH.THREE));
        database.addCard(new Card(Card.CARD_COLOR.RED, Card.CARD_WORTH.THREE));
        database.addCard(new Card(Card.CARD_COLOR.RED, Card.CARD_WORTH.THREE));
        database.addCard(new Card(Card.CARD_COLOR.RED, Card.CARD_WORTH.FOUR));
        database.addCard(new Card(Card.CARD_COLOR.RED, Card.CARD_WORTH.FOUR));
        database.addCard(new Card(Card.CARD_COLOR.RED, Card.CARD_WORTH.FOUR));
        database.addCard(new Card(Card.CARD_COLOR.RED, Card.CARD_WORTH.FOUR));

        database.addCard(new Card(Card.CARD_COLOR.PURPLE, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.PURPLE, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.PURPLE, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.PURPLE, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.PURPLE, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.PURPLE, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.PURPLE, Card.CARD_WORTH.THREE));
        database.addCard(new Card(Card.CARD_COLOR.PURPLE, Card.CARD_WORTH.THREE));
        database.addCard(new Card(Card.CARD_COLOR.PURPLE, Card.CARD_WORTH.FOUR));
        database.addCard(new Card(Card.CARD_COLOR.PURPLE, Card.CARD_WORTH.FOUR));
        database.addCard(new Card(Card.CARD_COLOR.PURPLE, Card.CARD_WORTH.FIVE));
        database.addCard(new Card(Card.CARD_COLOR.PURPLE, Card.CARD_WORTH.FIVE));

        database.addCard(new Card(Card.CARD_COLOR.GREY, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.GREY, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.GREY, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.GREY, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.GREY, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.GREY, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.GREY, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.GREY, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.GREY, Card.CARD_WORTH.FOUR));
        database.addCard(new Card(Card.CARD_COLOR.GREY, Card.CARD_WORTH.FOUR));
        database.addCard(new Card(Card.CARD_COLOR.GREY, Card.CARD_WORTH.FOUR));
        database.addCard(new Card(Card.CARD_COLOR.GREY, Card.CARD_WORTH.FOUR));

        database.addCard(new Card(Card.CARD_COLOR.GREEN, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.GREEN, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.GREEN, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.GREEN, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.GREEN, Card.CARD_WORTH.THREE));
        database.addCard(new Card(Card.CARD_COLOR.GREEN, Card.CARD_WORTH.THREE));
        database.addCard(new Card(Card.CARD_COLOR.GREEN, Card.CARD_WORTH.FOUR));
        database.addCard(new Card(Card.CARD_COLOR.GREEN, Card.CARD_WORTH.FOUR));
        database.addCard(new Card(Card.CARD_COLOR.GREEN, Card.CARD_WORTH.FIVE));
        database.addCard(new Card(Card.CARD_COLOR.GREEN, Card.CARD_WORTH.FIVE));
        database.addCard(new Card(Card.CARD_COLOR.GREEN, Card.CARD_WORTH.FIVE));
        database.addCard(new Card(Card.CARD_COLOR.GREEN, Card.CARD_WORTH.FIVE));

        database.addCard(new Card(Card.CARD_COLOR.BLUE, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.BLUE, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.BLUE, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.BLUE, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.BLUE, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.BLUE, Card.CARD_WORTH.TWO));
        database.addCard(new Card(Card.CARD_COLOR.BLUE, Card.CARD_WORTH.THREE));
        database.addCard(new Card(Card.CARD_COLOR.BLUE, Card.CARD_WORTH.THREE));
        database.addCard(new Card(Card.CARD_COLOR.BLUE, Card.CARD_WORTH.THREE));
        database.addCard(new Card(Card.CARD_COLOR.BLUE, Card.CARD_WORTH.THREE));
        database.addCard(new Card(Card.CARD_COLOR.BLUE, Card.CARD_WORTH.FIVE));
        database.addCard(new Card(Card.CARD_COLOR.BLUE, Card.CARD_WORTH.FIVE));

        database.ramdomize();

        return database;
    }

}
