package hk.edu.polyu.comp4512.card;

import java.util.ArrayList;

import hk.edu.polyu.comp4512.card.cards_database.Database;

/**
 * Created by hopkins on 29/11/2017.
 */

public class Player {
    private Database pile;
    private int money;
    private int defToken;
    private ArrayList<String> eventLog;

    public Player() {
        pile = new Database();
        money = 5;
        defToken = 1;
        eventLog = new ArrayList<>();
    }

    public int getMoney() {
        return money;
    }

    public int getDefToken() {
        return defToken;
    }

    public void addMoney(int money) {
        this.money += money;
    }

    public void deductMoney(int money) {
        this.money -= money;
    }

    public void addDefenseToken(int token) {
        this.defToken += token;
    }

    public void deductDefenseToken(int token) {
        this.defToken -= token;
    }

    public Database getPile() {
        return pile;
    }

    public void addEventLog(String evt) {
        eventLog.add(evt);
    }

    public ArrayList<String> getEventLog() {
        return eventLog;
    }

    public void clearEventLog() {
        eventLog.clear();
    }

}
