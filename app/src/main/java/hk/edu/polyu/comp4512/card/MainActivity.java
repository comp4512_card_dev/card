package hk.edu.polyu.comp4512.card;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button btnStartGame;
    RadioButton rbTwoPlayers, rbThreePlayers, rbFourPlayers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btnStartGame = (Button)findViewById(R.id.btnStartGame);
        rbTwoPlayers = (RadioButton)findViewById(R.id.rbTwoPlayers);
        rbThreePlayers = (RadioButton)findViewById(R.id.rbThreePlayers);
        rbFourPlayers = (RadioButton)findViewById(R.id.rbFourPlayers);

        btnStartGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(rbTwoPlayers.isChecked()) {
                    // Start game with two players
                    startGame(2);
                }else if(rbThreePlayers.isChecked()) {
                    // Start game with three players
                    startGame(3);
                }else if(rbFourPlayers.isChecked()) {
                    // Start game with four players
                    startGame(4);
                }else{
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Please select number of players").setCancelable(false);
                    builder.setPositiveButton("OK", null);
                    builder.show();
                }
            }
        });
    }

    private void startGame(int numberOfPlayers) {
        Toast.makeText(getActivity(), "Starting IP CLASH with players: " + numberOfPlayers, Toast.LENGTH_LONG).show();
        Intent intent = new Intent(getActivity(), GameActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("number_of_players", numberOfPlayers);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }

    private Activity getActivity() {
        return this;
    }
}
