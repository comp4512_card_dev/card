package hk.edu.polyu.comp4512.card;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import hk.edu.polyu.comp4512.card.cards_database.Card;

/**
 * Created by hopkins on 30/11/2017.
 */

public class DialogBoxes {
    public static void AskAndAct(final String methodName, String actionName, int cost, final Activity activity) {
        AskAndAct(methodName, actionName, cost, null, activity);
    }
    public static void AskAndAct(final String methodName, String actionName, int cost, final Card card, final Activity activity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage("You are going to perform the action \"" + actionName + "\", this will cause you $" + cost + "M.\n\nProceed?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if(card == null) {
                            if(methodName.equals("EspionagePlayer")) ((GameActivity)activity).EspionagePlayer();
                            if(methodName.equals("RD")) ((GameActivity)activity).RD();
                            if(methodName.equals("Defense")) ((GameActivity)activity).Defense();
                        }else{
                            if(methodName.equals("SuePlayer")) ((GameActivity)activity).SuePlayer(card);
                            if(methodName.equals("Patent")) ((GameActivity)activity).Patent(card);
                            if(methodName.equals("OpenSource")) ((GameActivity)activity).OpenSource(card);
                        }
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    public static void GenericDialogBox(String message, Activity activity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setMessage(message).setCancelable(false);
        builder.setPositiveButton("OK", null);

        builder.show();
    }

    static void ShowCardOption(final Card card, int ownedCount, final Activity activity) {
        final Dialog dialog = new Dialog(activity, R.style.Theme_AppCompat_Light_DarkActionBar);
        dialog.setContentView(R.layout.dialog_select_ip);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        ImageView ivDialogCard = dialog.findViewById(R.id.ivDialogCard);
        ivDialogCard.setImageDrawable(card.getFaceUpDrawable(activity.getResources()));

        TextView tvDialogColor = dialog.findViewById(R.id.tvDialogColor);
        TextView tvDialogPatentedIncome = dialog.findViewById(R.id.tvDialogPatentedIncome);
        TextView tvDialogStatus = dialog.findViewById(R.id.tvDialogStatus);
        TextView tvDialogOwnedCount = dialog.findViewById(R.id.tvDialogOwnedCount);

        tvDialogColor.setText("Color: " + card.getColor().toString());
        tvDialogPatentedIncome.setText("Income per round if patented//open src.: $" + (card.getWorth().getNumber()+(ownedCount-1)*1) + "M//$" + (ownedCount*1) + "M");
        tvDialogStatus.setText("IP Status: " + card.getCardStatus().toString().replace('_', ' '));
        tvDialogOwnedCount.setText("Owned Count: " + ownedCount);

        Button btnDialogPatent = dialog.findViewById(R.id.btnDialogPatent);
        Button btnDialogOpenSource = dialog.findViewById(R.id.btnDialogOpenSource);
        Button btnDialogSueOthers = dialog.findViewById(R.id.btnDialogSueOthers);
        Button btnDialogClose = dialog.findViewById(R.id.btnDialogClose);

        if(card.cardFacedUp()) {
            btnDialogPatent.setEnabled(false);
            btnDialogOpenSource.setEnabled(false);
        }

        btnDialogPatent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AskAndAct("Patent", "Patent", 3, card, activity);
                dialog.dismiss();
            }
        });

        btnDialogOpenSource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AskAndAct("OpenSource", "OpenSource", 0, card, activity);
                dialog.dismiss();
            }
        });

        btnDialogSueOthers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AskAndAct("SuePlayer", "Suing Player", 2, card, activity);
            }
        });

        btnDialogClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    static void ShowOtherPlayers(final int currentPlayer, final int totalPlayer, Player[] players, final Activity activity) {
        ShowOtherPlayers(currentPlayer, totalPlayer, players, activity, false, false);
    }

    static void ShowOtherPlayers(final int currentPlayer, final int totalPlayer, Player[] players, final Activity activity, boolean espionage, boolean suing) {
        final Dialog dialog = new Dialog(activity, R.style.Theme_AppCompat_Light_DarkActionBar);
        dialog.setContentView(R.layout.dialog_other_players);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        TextView tvOtherPlayersPlayerOneHeader = dialog.findViewById(R.id.tvOtherPlayersPlayerOneHeader);
        TextView tvOtherPlayersPlayerTwoHeader = dialog.findViewById(R.id.tvOtherPlayersPlayerTwoHeader);
        TextView tvOtherPlayersPlayerThreeHeader = dialog.findViewById(R.id.tvOtherPlayersPlayerThreeHeader);
        TextView tvOtherPlayersPlayerFourHeader = dialog.findViewById(R.id.tvOtherPlayersPlayerFourHeader);

        LinearLayout llOtherPlayersPlayerOne = dialog.findViewById(R.id.llOtherPlayersPlayerOne);
        LinearLayout llOtherPlayersPlayerTwo = dialog.findViewById(R.id.llOtherPlayersPlayerTwo);
        LinearLayout llOtherPlayersPlayerThree = dialog.findViewById(R.id.llOtherPlayersPlayerThree);
        LinearLayout llOtherPlayersPlayerFour = dialog.findViewById(R.id.llOtherPlayersPlayerFour);

        if(totalPlayer == 2) {
            tvOtherPlayersPlayerThreeHeader.setVisibility(View.INVISIBLE);
            tvOtherPlayersPlayerFourHeader.setVisibility(View.INVISIBLE);
            llOtherPlayersPlayerThree.setVisibility(View.INVISIBLE);
            llOtherPlayersPlayerFour.setVisibility(View.INVISIBLE);
        }else if(totalPlayer == 3) {
            tvOtherPlayersPlayerFourHeader.setVisibility(View.INVISIBLE);
            llOtherPlayersPlayerFour.setVisibility(View.INVISIBLE);
        }

        LinearLayout llToDrawFullDisclosure = null; // currentPlayer's pile should be shown in full
        for(int i=0; i<totalPlayer; i++) {
            if(i == 0) {
                if(currentPlayer-1 == i) {
                    if(!espionage) {
                        tvOtherPlayersPlayerOneHeader.setText("Player 1 (YOU)");
                    }else if(espionage) {
                        tvOtherPlayersPlayerOneHeader.setText("Please select target's card to perform espionage.");
                    }
                    if(suing) {
                        tvOtherPlayersPlayerOneHeader.setText("Please select target's card to perform legal action.");
                    }
                    llToDrawFullDisclosure=llOtherPlayersPlayerOne;
                }else{
                    llOtherPlayersPlayerOne.removeAllViews(); // clear ui display for this player
                    CardDrawer.getOpponentLinearLayout(players[i].getPile().getCards(), llOtherPlayersPlayerOne, activity, espionage, 1, suing, 1, dialog);
                }
            }else if(i == 1) {
                if(currentPlayer-1 == i) {
                    if(!espionage) {
                        tvOtherPlayersPlayerTwoHeader.setText("Player 2 (YOU)");
                    }else if(espionage){
                        tvOtherPlayersPlayerTwoHeader.setText("Please select target's card to perform espionage.");
                    }
                    if(suing) {
                        tvOtherPlayersPlayerTwoHeader.setText("Please select target's card to perform legal action.");
                    }
                    llToDrawFullDisclosure=llOtherPlayersPlayerTwo;
                }else{
                    llOtherPlayersPlayerTwo.removeAllViews(); // clear ui display for this player
                    CardDrawer.getOpponentLinearLayout(players[i].getPile().getCards(), llOtherPlayersPlayerTwo, activity, espionage, 2, suing, 2, dialog);
                }
            }else if(i == 2) {
                if(currentPlayer-1 == i) {
                    if(!espionage) {
                        tvOtherPlayersPlayerThreeHeader.setText("Player 3 (YOU)");
                    }else if(espionage){
                        tvOtherPlayersPlayerThreeHeader.setText("Please select target's card to perform espionage.");
                    }
                    if(suing) {
                        tvOtherPlayersPlayerThreeHeader.setText("Please select target's card to perform legal action.");
                    }
                    llToDrawFullDisclosure=llOtherPlayersPlayerThree;
                }else{
                    llOtherPlayersPlayerThree.removeAllViews(); // clear ui display for this player
                    CardDrawer.getOpponentLinearLayout(players[i].getPile().getCards(), llOtherPlayersPlayerThree, activity, espionage, 3, suing, 3, dialog);

                }
            }else if(i == 3) {
                if(currentPlayer-1 == i) {
                    if(!espionage) {
                        tvOtherPlayersPlayerFourHeader.setText("Player 4 (YOU)");
                    }else if(espionage){
                        tvOtherPlayersPlayerFourHeader.setText("Please select target's card to perform espionage.");
                    }
                    if(suing) {
                        tvOtherPlayersPlayerFourHeader.setText("Please select target's card to perform legal action.");
                    }
                    llToDrawFullDisclosure=llOtherPlayersPlayerFour;
                }else{
                    llOtherPlayersPlayerFour.removeAllViews(); // clear ui display for this player
                    CardDrawer.getOpponentLinearLayout(players[i].getPile().getCards(), llOtherPlayersPlayerFour, activity, espionage, 4, suing, 4, dialog);
                }
            }
        }

        if(llToDrawFullDisclosure != null && !espionage && !suing) {
            for (Card card : players[currentPlayer-1].getPile().getCards()) {
                ImageView imageView = new ImageView(activity);
                imageView.setAdjustViewBounds(true);

                if(card.getCardStatus() == Card.CARD_STATUS.TRADE_SECRET) {
                    imageView = CardDrawer.drawOnImageView(card, imageView, true, "TRADE SECRET", Color.YELLOW, activity);
                }else if(card.getCardStatus() == Card.CARD_STATUS.PATENTED) {
                    imageView = CardDrawer.drawOnImageView(card, imageView, false, "PATENTED", Color.GREEN, activity); // face up
                }else if(card.getCardStatus() == Card.CARD_STATUS.PATENT_PENDING) {
                    imageView = CardDrawer.drawOnImageView(card, imageView, false, "PATENT PENDING", Color.RED, activity); // face up
                }else if(card.getCardStatus() == Card.CARD_STATUS.OPEN_SOURCED) {
                    imageView = CardDrawer.drawOnImageView(card, imageView, false, "OPEN SOURCED", Color.BLACK, activity); // face up
                }
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(20, 0, 20, 0);
                llToDrawFullDisclosure.addView(imageView,layoutParams);
            }
        }

        (dialog.findViewById(R.id.btnOtherPlayersDialogClose)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });



        dialog.show();
    }
}
