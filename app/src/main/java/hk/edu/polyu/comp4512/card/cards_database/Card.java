package hk.edu.polyu.comp4512.card.cards_database;


import android.content.res.Resources;
import android.graphics.drawable.Drawable;

/**
 * Created by hopkins on 29/11/2017.
 */

public class Card {
    public enum CARD_COLOR {
        BLUE, GREEN, GREY, PURPLE, RED
    }
    public enum CARD_WORTH {
        TWO(2), THREE(3), FOUR(4), FIVE(5);

        private final int value;
        CARD_WORTH(int value) {
            this.value = value;
        }
        public int getNumber() {
            return value;
        }
    }
    public enum CARD_STATUS {
        TRADE_SECRET, PATENTED, PATENT_PENDING, OPEN_SOURCED, IN_GLOBAL_PILE
    }


    private CARD_COLOR color;
    private CARD_WORTH worth;
    private CARD_STATUS cardStatus;

    public Card(CARD_COLOR color, CARD_WORTH worth) {
        this.color = color;
        this.worth = worth;
        this.cardStatus = CARD_STATUS.IN_GLOBAL_PILE;
    }

    public boolean isSameWith(Card card) {
        return (this.getWorth() == card.getWorth() && this.getColor() == card.getColor());
    }

    public CARD_COLOR getColor() {
        return this.color;
    }

    public CARD_WORTH getWorth() {
        return this.worth;
    }

    public CARD_STATUS getCardStatus() {
        return this.cardStatus;
    }

    public boolean cardFacedUp() {
        return (this.cardStatus == CARD_STATUS.PATENTED || this.cardStatus == CARD_STATUS.PATENT_PENDING || this.cardStatus == CARD_STATUS.OPEN_SOURCED);
    }

    public void setCardStatus(CARD_STATUS cardStatus) {
        this.cardStatus = cardStatus;
    }

    public Drawable getFaceUpDrawable(Resources res) {
        int worth_int=0;

        if(worth == CARD_WORTH.TWO) worth_int=2;
        if(worth == CARD_WORTH.THREE) worth_int=3;
        if(worth == CARD_WORTH.FOUR) worth_int=4;
        if(worth == CARD_WORTH.FIVE) worth_int=5;

        String id = this.color.toString().toLowerCase()+"_"+worth_int;
        Drawable drawable = res.getDrawable(res.getIdentifier(id, "drawable", "hk.edu.polyu.comp4512.card"));

        return drawable;
    }
    public Drawable getFaceDownDrawable(Resources res) {
        String id = this.color.toString().toLowerCase();
        Drawable drawable = res.getDrawable(res.getIdentifier(id, "drawable", "hk.edu.polyu.comp4512.card"));

        return drawable;
    }

}
