package hk.edu.polyu.comp4512.card;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;

import hk.edu.polyu.comp4512.card.cards_database.Card;
import hk.edu.polyu.comp4512.card.cards_database.Database;

public class GameActivity extends AppCompatActivity {
    Database globalPile;
    int currentPlayer;
    int totalPlayer;
    int currentRound;

    Button btnNextPlayer, btnExitGame, btnShowMyCards, btnShowOthersCards;
    Player[] players;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        Bundle bundle = getIntent().getExtras();
        int value = -1;
        if(bundle != null) value = bundle.getInt("number_of_players");

        if(value == -1) {
            // Something went wrong
            finish();
        }

        // Game context stuff
        currentPlayer=1;
        totalPlayer=value;
        currentRound=1;
        globalPile = Database.generateDefaultDeck();
        players = new Player[totalPlayer];

        // Initialize each player and draw two cards for each player
        for(int i=0; i<players.length; i++) {
            players[i] = new Player();
            players[i].getPile().addCard(globalPile.drawOneCard());
            players[i].getPile().addCard(globalPile.drawOneCard());
        }

        // UI stuff
        btnNextPlayer = findViewById(R.id.btnNextPlayer);
        btnExitGame = findViewById(R.id.btnExitGame);
        btnShowMyCards = findViewById(R.id.btnShowMyCards);
        btnShowOthersCards = findViewById(R.id.btnShowOthersCards);

        // Click listeners
        btnNextPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextPlayer();
                disableEverything();
                btnShowMyCards.setVisibility(View.VISIBLE);
                ((Button)findViewById(R.id.btnShowMyCards)).setText("I AM PLAYER " + currentPlayer + ", SHOW MY CARD PLEASE.");
            }
        });
        btnExitGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Are you sure want to exit the game?")
                        .setCancelable(false)
                        .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                getActivity().finish();
                            }
                        })
                        .setNegativeButton("NO", null)
                        .show();
            }
        });

        btnShowMyCards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnShowMyCards.setVisibility(View.INVISIBLE);
                enableEverything();
                renderPlayer();

                // Show event logs
                if(getCurrentPlayer().getEventLog().size() == 0) {
                    DialogBoxes.GenericDialogBox("No news is good news.", getActivity());
                }else{
                    int i=1;
                    for(String event : getCurrentPlayer().getEventLog()) {
                        DialogBoxes.GenericDialogBox("EVENT " + i + "/" + getCurrentPlayer().getEventLog().size() + ":\n" + event, getActivity());
                    }
                    getCurrentPlayer().clearEventLog();
                }
            }
        });

        btnShowOthersCards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogBoxes.ShowOtherPlayers(currentPlayer, totalPlayer, players, getActivity());
            }
        });



        disableEverything();
        ((TextView)findViewById(R.id.tvPlayer)).setText("You are player " + currentPlayer + "/" + totalPlayer);
        ((Button)findViewById(R.id.btnShowMyCards)).setText("I AM PLAYER " + currentPlayer + ", SHOW MY CARD PLEASE.");

    }

    // Used for transitioning to the next player
    private void disableEverything() {
        findViewById(R.id.btnActDefense).setVisibility(View.INVISIBLE);
        findViewById(R.id.btnActRD).setVisibility(View.INVISIBLE);
        findViewById(R.id.btnActEspionage).setVisibility(View.INVISIBLE);
        findViewById(R.id.btnShowOthersCards).setVisibility(View.INVISIBLE);
        findViewById(R.id.tvMyIncome).setVisibility(View.INVISIBLE);
        findViewById(R.id.tvMyStatus).setVisibility(View.INVISIBLE);
        findViewById(R.id.tvMyCardsHeader).setVisibility(View.INVISIBLE);
        findViewById(R.id.btnNextPlayer).setVisibility(View.INVISIBLE);
        findViewById(R.id.llMyCards).setVisibility(View.INVISIBLE);
    }

    private void enableEverything() {
        findViewById(R.id.btnActDefense).setVisibility(View.VISIBLE);
        findViewById(R.id.btnActRD).setVisibility(View.VISIBLE);
        findViewById(R.id.btnActEspionage).setVisibility(View.VISIBLE);
        findViewById(R.id.btnShowOthersCards).setVisibility(View.VISIBLE);
        findViewById(R.id.tvMyIncome).setVisibility(View.VISIBLE);
        findViewById(R.id.tvMyStatus).setVisibility(View.VISIBLE);
        findViewById(R.id.tvMyCardsHeader).setVisibility(View.VISIBLE);
        findViewById(R.id.btnNextPlayer).setVisibility(View.VISIBLE);
        findViewById(R.id.llMyCards).setVisibility(View.VISIBLE);
    }

    public void actionBtnPressed(View v) {
        if(v.getId() == R.id.btnActEspionage) DialogBoxes.AskAndAct("EspionagePlayer", "Espionage", 3, getActivity());
        if(v.getId() == R.id.btnActRD) DialogBoxes.AskAndAct("RD", "R&D", 5, getActivity());;
        if(v.getId() == R.id.btnActDefense) DialogBoxes.AskAndAct("Defense", "Defense Strategy", 2, getActivity());
    }

    private Card myCardToSue = null;
    public void SuePlayer(Card myCard) {
        if(getCurrentPlayer().getMoney() < 2) {
            DialogBoxes.GenericDialogBox("Sorry, you don't have enough cash, you need $2M.", getActivity());
            return;
        }

        if(myCard.getCardStatus() != Card.CARD_STATUS.PATENTED) {
            DialogBoxes.GenericDialogBox("Sorry, you can only sue others when your IP is patented.", getActivity());
            return;
        }

        myCardToSue=myCard;
        DialogBoxes.ShowOtherPlayers(currentPlayer, totalPlayer, players, getActivity(), false, true);
    }

    public void PerformSuingOnSelectedTarget(int targetPlayer, int targetCard) {
        Card.CARD_COLOR targetCardColor = players[targetPlayer-1].getPile().getCards().get(targetCard).getColor();

        if(targetCardColor != myCardToSue.getColor()) {
            DialogBoxes.GenericDialogBox("Sorry, you must select a IP card with same color (" + myCardToSue.getColor().toString() + ").", getActivity());
            return;
        }

        // Legal proceeding started, deduct money
        getCurrentPlayer().deductMoney(2);

        // Now roll the dice
        int diceVal = (int)(Math.random()* 5)+1; // 1-6

        if(players[targetPlayer-1].getDefToken() >= diceVal) { // defense successful
            DialogBoxes.GenericDialogBox("The opponent is too strong, you failed to sue them.", getActivity());
            players[targetPlayer-1].addEventLog("Player " + currentPlayer + " tried to sue you, but your layer is too strong!");
        }else{ // Can sue, now compare two cards
            Card target = players[targetPlayer-1].getPile().getCards().get(targetCard);
            if(target.isSameWith(myCardToSue)) { // Wow they match, we revoke that card, and penalize the loser
                DialogBoxes.GenericDialogBox("They are GUILTY! You won $2M! (And no court fee needed!)", getActivity());
                players[targetPlayer-1].addEventLog("Player " + currentPlayer + " tried to sue you, and they won! One IP Card is withdrawn and -$4M balance.");
                players[targetPlayer-1].deductMoney(4); // Draw $2M from them to you
                getCurrentPlayer().addMoney(2); // Restore the fee for legal proceedings
                getCurrentPlayer().addMoney(2); // Compensation

                // Move that card to the bottom of the pile
                players[targetPlayer-1].getPile().getCards().remove(targetCard);
                globalPile.addCard(target);
            }else{
                // Not match
                DialogBoxes.GenericDialogBox("Too bad, their IP is the " + target.getColor().toString() + " colored card worth $" + target.getWorth().getNumber() + "M per year, not matched with yours.\nYou lose $2M.", getActivity());
                players[targetPlayer-1].addEventLog("Player " + currentPlayer + " tried to sue you, but they are wronged about you.");
            }
        }
        renderPlayer();
    }

    public void EspionagePlayer() {
        if(getCurrentPlayer().getMoney() < 3) {
            DialogBoxes.GenericDialogBox("Sorry, you don't have enough cash, you need $3M.", getActivity());
            return;
        }

        DialogBoxes.ShowOtherPlayers(currentPlayer, totalPlayer, players, getActivity(), true, false);

    }

    public void PerformEspionageOnSelectedTarget(int targetPlayer, int targetCard) {
        // Deduct the money first
        getCurrentPlayer().deductMoney(3);

        // Now roll the dice
        int diceVal = (int)(Math.random()* 5)+1; // 1-6

        if(players[targetPlayer-1].getDefToken() >= diceVal) { // defense successful
            DialogBoxes.GenericDialogBox("You lose, espionage unsuccessful.", getActivity());
            players[targetPlayer-1].addEventLog("Player " + currentPlayer + " tried to steal something from you, but our security is too good, no worries. However, we lost one defense token.");
        }else{ // Espionage wins!!
            Card stolenCard = players[targetPlayer-1].getPile().getCards().get(targetCard);
            players[targetPlayer-1].getPile().getCards().remove(targetCard);
            getCurrentPlayer().getPile().addCard(stolenCard);
            DialogBoxes.GenericDialogBox("You won, you have stolen the IP. Great work!", getActivity());
            players[targetPlayer-1].addEventLog("Player " + currentPlayer + " tried to steal something from you, and they got it! We should enhance our defense!");
        }
        if(players[targetPlayer-1].getDefToken() > 0) players[targetPlayer-1].deductDefenseToken(1); // Defender always loses 1 defense token

        renderPlayer();
    }

    public void RD() {
        if(getCurrentPlayer().getMoney() < 5) {
            DialogBoxes.GenericDialogBox("Sorry, you don't have enough cash, you need $5M.", getActivity());
            return;
        }
        if(globalPile.getSize() == 0) {
            DialogBoxes.GenericDialogBox("Sorry, no more cards from the pile.", getActivity());
            return;
        }

        getCurrentPlayer().deductMoney(5);
        Card newCard = globalPile.drawOneCard();

        // Check if newCard already in the current pile
        ArrayList<Card> cards = getCurrentPlayer().getPile().getCards();
        for(int i=0; i<cards.size(); i++) {
            if(cards.get(i).isSameWith(newCard)) {
                newCard.setCardStatus(cards.get(i).getCardStatus()); // Set the status to the same
                break;
            }
        }

        getCurrentPlayer().getPile().addCard(newCard);

        renderPlayer();

    }

    public void Defense() {
        if(getCurrentPlayer().getMoney() < 2) {
            DialogBoxes.GenericDialogBox("Sorry, you don't have enough cash, you need $2M.", getActivity());
            return;
        }

        if(getCurrentPlayer().getDefToken() == 5) {
            DialogBoxes.GenericDialogBox("Sorry, you already have the maximum of 5 defense token.", getActivity());
            return;
        }

        getCurrentPlayer().deductMoney(2);
        getCurrentPlayer().addDefenseToken(1);

        renderPlayer();

    }

    public void Patent(Card card) {
        if(getCurrentPlayer().getMoney() < 3) {
            DialogBoxes.GenericDialogBox("Sorry, you don't have enough cash, you need $3M.", getActivity());
            return;
        }

        // Check other players have FACED UP this card
        for(int i=0; i<totalPlayer; i++) {
            if(i == (currentPlayer-1)) continue;
            if(players[i].getPile().facedUpDuplicated(card)) {
                // Duplication found
                DialogBoxes.GenericDialogBox("Sorry, player " + (i+1) + " already have the same IP being patented/already patented/open sourced, you can't patent it" + ".", getActivity());
                return;
            }
        }

        // Change status to patent pending, and face up this card
        getCurrentPlayer().deductMoney(3);
        ArrayList<Card> cards = getCurrentPlayer().getPile().getCards();
        for(int i=0; i<cards.size(); i++) {
            if(cards.get(i).isSameWith(card)) {
                cards.get(i).setCardStatus(Card.CARD_STATUS.PATENT_PENDING);
            }
        }
        renderPlayer();
    }

    public void OpenSource(Card card) {
        // Check other players have FACED UP this card
        for(int i=0; i<totalPlayer; i++) {
            if(i == (currentPlayer-1)) continue;
            if(players[i].getPile().facedUpDuplicated(card)) {
                // Duplication found
                DialogBoxes.GenericDialogBox("Sorry, player " + (i+1) + " already have the same IP being patented/already patented/open sourced, you can't open source it" + ".", getActivity());
                return;
            }
        }
        // Change status to open sourced
        ArrayList<Card> cards = getCurrentPlayer().getPile().getCards();
        for(int i=0; i<cards.size(); i++) {
            if(cards.get(i).isSameWith(card)) {
                cards.get(i).setCardStatus(Card.CARD_STATUS.OPEN_SOURCED);
            }
        }
        renderPlayer();
    }

    private Player getCurrentPlayer() {
        return players[currentPlayer-1];
    }

    private int getIncomeNextRound(Player player) {
        // Income = Patent Card Value +
        // Open Source Cards +
        // Patent pending +
        // Stacked patents

        int totalIncome=0;

        ArrayList<Card> cards = player.getPile().getCards();
        ArrayList<Card> cardsDistnct = new ArrayList<>();
        ArrayList<Integer> countsInCardsDistinct = new ArrayList<>();

        for(Card c : cards) {
            boolean found=false;
            for(Card cd : cardsDistnct) {
                if(cd.isSameWith(c)) {
                    found=true;
                    break;
                }
            }
            if(!found) {
                cardsDistnct.add(c);
            }
        }

        for(Card card : cardsDistnct) {
            // check "card" in "cards"
            int cnt=0;
            for(Card c : cards) {
                if(card.isSameWith(c)) cnt++;
            }
            countsInCardsDistinct.add(cnt);
        }

        for(int i=0; i<cardsDistnct.size(); i++) {
            Card card = cardsDistnct.get(i);
            int countForThisCard = countsInCardsDistinct.get(i);

            if(card.getCardStatus() == Card.CARD_STATUS.PATENTED) {
                // BaseValue + 1*(Count-1)
                totalIncome += card.getWorth().getNumber() + 1*(countForThisCard-1);
            }else if(card.getCardStatus() == Card.CARD_STATUS.PATENT_PENDING) {
                // 1*Count
                totalIncome += 1*countForThisCard;
            }else if(card.getCardStatus() == Card.CARD_STATUS.OPEN_SOURCED) {
                // 1*Count
                totalIncome += 1*countForThisCard;
            }else if(card.getCardStatus() == Card.CARD_STATUS.TRADE_SECRET) {
                // 1*Count
                totalIncome += 1*countForThisCard;
            }

        }



        return totalIncome;
    }



    private void renderPlayer() {
        TextView playerCntView = findViewById(R.id.tvPlayer);
        playerCntView.setText("You are player " + currentPlayer + "/" + totalPlayer);

        // Player reference
        Player player = getCurrentPlayer();

        // UI Declarations
        LinearLayout llMyCards = findViewById(R.id.llMyCards);
        TextView tvMyStatus = findViewById(R.id.tvMyStatus);
        TextView tvMyIncome = findViewById(R.id.tvMyIncome);

        // Render player status
        tvMyStatus.setText("Round=" + currentRound + "  Money=$" + player.getMoney() + "M  Defense Token=" + player.getDefToken());

        // Render player income next round
        tvMyIncome.setText("Income in next round=$" + getIncomeNextRound(getCurrentPlayer()) + "M");

        // Render player cards
        final ArrayList<Card> cards = player.getPile().getCards();
        llMyCards.removeAllViews(); // clear ui display for this player

        int cardIndex=0;
        for(Card card : cards) {
            ImageView imageView = new ImageView(getActivity());
            imageView.setAdjustViewBounds(true);

            if(card.getCardStatus() == Card.CARD_STATUS.TRADE_SECRET) {
                imageView = CardDrawer.drawOnImageView(card, imageView, true, "TRADE SECRET", Color.YELLOW, getActivity());
            }else if(card.getCardStatus() == Card.CARD_STATUS.PATENTED) {
                imageView = CardDrawer.drawOnImageView(card, imageView, false, "PATENTED", Color.GREEN, getActivity()); // face up
            }else if(card.getCardStatus() == Card.CARD_STATUS.PATENT_PENDING) {
                imageView = CardDrawer.drawOnImageView(card, imageView, false, "PATENT PENDING", Color.RED, getActivity()); // face up
            }else if(card.getCardStatus() == Card.CARD_STATUS.OPEN_SOURCED) {
                imageView = CardDrawer.drawOnImageView(card, imageView, false, "OPEN SOURCED", Color.BLACK, getActivity()); // face up
            }

            final int clickedCardIndex = cardIndex;
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Card card = cards.get(clickedCardIndex);
                    DialogBoxes.ShowCardOption(card, getCurrentPlayer().getPile().countCard(card), getActivity());
                }
            });

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(20, 0, 20, 0);
            llMyCards.addView(imageView,layoutParams);

            cardIndex++;
        }

    }

    private void nextPlayer() {
        currentPlayer++;
        if(currentPlayer > totalPlayer) {
            endRound();
        }
        renderPlayer();

    }

    private void endRound() {
        currentPlayer = 1;
        currentRound++;

        for(int i=0; i<totalPlayer; i++) {
            // Add income
            int income = getIncomeNextRound(players[i]);
            players[i].addMoney(income);

            // Patent-pending becomes patented
            for(Card c : players[i].getPile().getCards()) {
                if(c.getCardStatus() == Card.CARD_STATUS.PATENT_PENDING) {
                    c.setCardStatus(Card.CARD_STATUS.PATENTED);
                }
            }
        }

    }

    private Activity getActivity() {
        return this;
    }
}
